<!-- Content Wrapper. Contains page content -->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			{title}
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Sarana</a></li>
			<li class="active">{title}</li>
		</ol>
	</section>

	<!-- Main content -->
	

	<section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-danger btn-sm" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
              <form role="form" action="{url}panel/sarana/{action}" method="post" enctype="multipart/form-data">
              	<input type="hidden" name="id_sarana" value="{id_sarana}">
              	<div class="col-md-12">
                <div class="form-group">
                  <label for="judul">Judul</label>
                  <input type="judul" class="form-control" id="judul" name="judul" placeholder="Judul" value="{judul}">
                </div>
            	  </div>
                <div class="col-md-12">
                <div class="form-group">
                  <label for="exampleInputFile">Gambar</label>
                  <input type="file" id="exampleInputFile" name="img" value="upload">
                  <img src="<?php echo base_url().'upload/'.$img ?>" width="100" height="100">
                 <p style="color: red">Ekstensi yang diperbolehkan .png | .jpg | .jpeg | .gif </p>
                </div>
                </div>
                <div class="col-md-12">
                	<div class="form-group">
                    <textarea id="editor1" name="isi" rows="10" cols="80">{isi}
                    </textarea>
                	</div>
                </div>
                <div class="box-footer">
                	<button type="submit" class="btn btn-primary">Submit</button>
                	<button type="reset" class="btn btn-box-tool">Reset</button>
             	</div>
              </form>
            </div>
          </div>
          <!-- /.box -->

      </div>
      <!-- ./row -->
    </section>

	<!-- /.content -->
</div>
</div>
</body>
<!-- /.content-wrapper -->