<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sarana extends CI_Controller {


  function __construct()
	{
		parent::__construct();

    $this->load->model('M_Sarana');

    define('uriClass', 'panel/sarana');
    $this->load->library('upload');
    $this->load->dbforge();
	}

	function index()
	{
    $data = array(
      'sarana'  => $this->M_Sarana->getData()
    ); 

    $data['url'] = BASE_URL;
		$data['title'] = 'Sarana';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'index';

		$this->panel_default->_render_page($data);
  }

  function add()
	{
    $data = array(
      'id_berita'  => '',
      'judul'  => '',
      'img'  => '',
      'isi'  => ''     
    );

    $data['action'] = 'add_save';
    $data['url'] = BASE_URL;
		$data['title'] = 'Tambah Sarana';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'form';
		
    $this->panel_default->_render_page($data);
  }

    function edit()
	{
	  $id_sarana = $this->uri->segment(4);

    $dataById = $this->M_Sarana->getDataById($id_sarana);

    $data = array(
        'id_sarana' => $dataById[0]->id_sarana,
        'judul' => $dataById[0]->judul,
        'img' => $dataById[0]->img,
        'isi' => $dataById[0]->isi
    );

    $data['action'] = 'edit_save';
    $data['url'] = BASE_URL;
  	$data['title'] = 'Dashboard | Edit';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'formEdit';
		$this->panel_default->_render_page($data);
  

  }

    function add_save()
    {
        $data = array(
          'id_sarana'  => $this->input->post('id_sarana',TRUE),
          'judul'  => $this->input->post('judul',TRUE),
          'img'  => $this->upload_file('img','jpg|jpeg|png|gif'),
          'isi'  => $this->input->post('isi',TRUE)     
        );

        $insert = $this->db->insert('sarana', $data);

        if($insert == TRUE):
          $this->session->set_flashdata('message', 'Input Berhasil');
        else:
          $this->session->set_flashdata('message', 'Input Gagal');
        endif;

        redirect(BASE_URL.uriClass);  
    }

    function edit_save()
    {
        $where = array(
          'id_sarana'  => $this->input->post('id_sarana',TRUE)
        );

        $data = array(
          'judul'  => $this->input->post('judul',TRUE),
          'img'  => $this->upload_file('img','jpg|jpeg|png|gif'),
          'isi'  => $this->input->post('isi',TRUE)     
        );

        $update = $this->db
        ->where($where)
        ->update('sarana', $data);

        if($update == TRUE):
          $this->session->set_flashdata('message', 'Edit Berhasil');
        else:
          $this->session->set_flashdata('message', 'Edit Gagal');
        endif;

        redirect(BASE_URL.uriClass);  

    }

    function delete()
    {
      $id_sarana = $this->uri->segment(4);
      $where = array(
          'id_sarana'  => $id_sarana
      );

      $delete = $this->db
        ->where($where)
        ->delete('sarana');

      if($delete == TRUE):
          $this->session->set_flashdata('message', 'Hapus Berhasil');
        else:
          $this->session->set_flashdata('message', 'Hapus Gagal');
        endif;

      redirect(BASE_URL.uriClass);  

    }

    public function upload_file($attribute, $extension)
    {
        $fileName = $this->input->post($attribute);
        $config['upload_path'] = './upload/';
        $config['file_name'] = $fileName;
        $config['allowed_types'] = $extension;
        $config['max_size'] = 10000;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (!$this->upload->do_upload($attribute)) {
            return $this->upload->display_errors();
        } else {
            return $this->upload->data()['file_name'];
        }
    }


}
