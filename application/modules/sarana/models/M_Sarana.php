<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Sarana extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }

    function getData() {
        $return = $this->db->get('sarana')->result();

        return $return;
    }

    function getDataById($id_sarana) {
        $where = array(
            'id_sarana' => $id_sarana
        );

        $result = $this->db
            ->where($where)
            ->get('sarana')
            ->result();

        return $result;
    }
}
