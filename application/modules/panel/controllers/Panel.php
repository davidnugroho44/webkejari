<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Panel extends CI_Controller {


  function __construct()
    {
        parent::__construct();

        $this->load->model('m_panel');

        define('uriClass', 'panel/panel');
    }


	function index()
	{
		// $dtTransaksi = $this->m_panel->getData();
  //       $countTransaksi = count($dtTransaksi); 

  //       $dtMenu = $this->m_panel->getDataMenu();
  //       $countMenu = count($dtMenu);

  //       $dtBahan = $this->m_panel->getDataBahan();
  //       $countBahan = count($dtBahan);

  //       $dtKomposisi = $this->m_panel->getDataKomposisi();
  //       $countKomposisi = count($dtKomposisi);
        
  //       $data = [
  //       	'totalTransaksi' => $countTransaksi,
  //       	'totalMenu' => $countMenu,
  //       	'totalBahan' => $countBahan,
  //           'totalKomposisi' => $countKomposisi,
  //       ];

		$data['url'] = BASE_URL;
		$data['title'] = 'Dashboard | Kejari Salatiga';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'index';
		$data['action'] = 'widgets';

		$this->panel_default->_render_page($data);
    }


}


?>