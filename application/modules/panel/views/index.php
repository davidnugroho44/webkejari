<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
    	<br>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> {title}</a></li>
        </ol>
        <?php 
        if ($this->session->flashdata('message')) {
        ?>
            <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> <?=$this->session->flashdata('message')?></h4>
            </div>
        <?php  
        }
        ?>
    </section>


	<!-- Main content -->
	
	<!-- /.content -->
<!-- 	<section class="content"> -->
	 <!-- <div class="row"> -->
<!--         <div class="col-lg-3 col-xs-6"> -->
          <!-- small box -->
<!--           <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?= $totalTransaksi; ?></h3>

              <p>Data Transaksi</p>
            </div>
            <div class="icon">
              <i class="fa fa-shopping-cart"></i>
            </div>
            <a href="{url}panel/transaksi" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div> -->
        <!-- ./col -->
<!--         <div class="col-lg-3 col-xs-6"> -->
          <!-- small box -->
<!--           <div class="small-box bg-green">
            <div class="inner">
              <h3><?= $totalMenu; ?></h3>

              <p>Jumlah Menu</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="{url}panel/menu" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div> -->
        <!-- ./col -->
<!--         <div class="col-lg-3 col-xs-6"> -->
          <!-- small box -->
<!--           <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?= $totalBahan; ?></h3>

              <p>Jumlah Bahan</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="{url}panel/bahan" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div> -->
        <!-- ./col -->
<!--         <div class="col-lg-3 col-xs-6"> -->
          <!-- small box -->
<!--           <div class="small-box bg-red">
            <div class="inner">
              <h3><?= $totalKomposisi; ?></h3>

              <p>Jumlah Komposisi</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="{url}panel/komposisi" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div> -->
        <!-- ./col -->
      
      <!-- /.row -->
<!--   </section> -->
    <body>
    <section class="content">

		<!-- Default box -->
		<div class="box">
			<div class="box-body">
				<h4 class="box-title">Helloo.. :)</h4>
        <h3 class="box-title">Selamat Datang di Halaman Admin Kejaksaan Negeri Salatiga..</h3><br>
        <div class="text-center">
          <img src="{asset}/dist/img/logo_ks.png" class="img-fluid" class="mx-auto d-block" width="300px">
        </div><br><br>
			<!-- /.box-body -->
      </div>
		</div>
		<!-- /.box -->
	</section>
  </body>
</div>
<!-- /.content-wrapper -->
