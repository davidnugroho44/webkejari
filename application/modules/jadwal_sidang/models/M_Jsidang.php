<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Jsidang extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }

    function getData() {
        $return = $this->db->get('jadwal_sidang')->result();

        return $return;
    }

    function getDataById($id_jadwal) {
        $where = array(
            'id_jadwal' => $id_jadwal
        );

        $result = $this->db
            ->where($where)
            ->get('jadwal_sidang')
            ->result();

        return $result;
    }
}
