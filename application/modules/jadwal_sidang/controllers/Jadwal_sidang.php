<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_sidang extends CI_Controller {


  function __construct()
	{
		parent::__construct();

    $this->load->model('M_Jsidang');

    define('uriClass', 'panel/jadwal_sidang');
    $this->load->helper(array('form', 'url'));
	}

	function index()
	{
    $data = array(
      'jadwal_sidang'  => $this->M_Jsidang->getData()
    ); 

    $data['url'] = BASE_URL;
		$data['title'] = 'Jadwal Sidang';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'index';

		$this->panel_default->_render_page($data);
  }

  function add()
	{
    $data = array(
      'id_jadwal'  => '',
      'tgl'  => '',
      'terdakwa'  => '',
      'jpu'  => '',
      'hakim'  => '',
      'no_perkara'  => '',
      'agenda'  => '',
      'kat'  => ''      
    );

    $data['action'] = 'add_save';
    $data['url'] = BASE_URL;
		$data['title'] = 'Tambah Jadwal Sidang';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'form';
		
    $this->panel_default->_render_page($data);
  }

    function edit()
	{
	  $id_jadwal = $this->uri->segment(4);

    $dataById = $this->M_Jsidang->getDataById($id_jadwal);

    $data = array(
        'id_jadwal' => $dataById[0]->id_jadwal,
        'tgl' => $dataById[0]->tgl,
        'terdakwa' => $dataById[0]->terdakwa,
        'jpu' => $dataById[0]->jpu,
        'hakim' => $dataById[0]->hakim,
        'no_perkara' => $dataById[0]->no_perkara,
        'agenda' => $dataById[0]->agenda,
        'kat' => $dataById[0]->kat
    );

    $data['action'] = 'edit_save';
    $data['url'] = BASE_URL;
  	$data['title'] = 'Dashboard | Edit';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'form';
		$this->panel_default->_render_page($data);
  

  }

function add_save()
{
    $tgl = @$this->input->post('tgl',TRUE) ?: date('Y-m-d');

    $data = array(
      'id_jadwal'  => $this->input->post('id_jadwal',TRUE),
      'tgl'  => $tgl,
      'terdakwa'  => $this->input->post('terdakwa',TRUE),
      'jpu'  => $this->input->post('jpu',TRUE),
      'hakim'  => $this->input->post('hakim',TRUE),
      'no_perkara'  => $this->input->post('no_perkara',TRUE),
      'agenda'  => $this->input->post('agenda',TRUE),
      'kat'  => $this->input->post('kat',TRUE)      
    );

    $insert = $this->db->insert('jadwal_sidang', $data);

    if($insert == TRUE):
      $this->session->set_flashdata('message', 'Input Berhasil');
    else:
      $this->session->set_flashdata('message', 'Input Gagal');
    endif;

    redirect(BASE_URL.uriClass);  
}

function edit_save()
{
    $where = array(
      'id_jadwal'  => $this->input->post('id_jadwal',TRUE)
    );

    $data = array(
      'id_jadwal'  => $this->input->post('id_jadwal',TRUE),
      'tgl'  => $this->input->post('tgl',TRUE),
      'terdakwa'  => $this->input->post('terdakwa',TRUE),
      'jpu'  => $this->input->post('jpu',TRUE),
      'hakim'  => $this->input->post('hakim',TRUE),
      'no_perkara'  => $this->input->post('no_perkara',TRUE),
      'agenda'  => $this->input->post('agenda',TRUE),
      'kat'  => $this->input->post('kat',TRUE)         
    );

    $update = $this->db
    ->where($where)
    ->update('jadwal_sidang', $data);

    if($update == TRUE):
      $this->session->set_flashdata('message', 'Edit Berhasil');
    else:
      $this->session->set_flashdata('message', 'Edit Gagal');
    endif;

    redirect(BASE_URL.uriClass);  

}

function delete()
{
  $id_jadwal = $this->uri->segment(4);
  $where = array(
      'id_jadwal'  => $id_jadwal
  );

  $delete = $this->db
    ->where($where)
    ->delete('jadwal_sidang');

  if($delete == TRUE):
      $this->session->set_flashdata('message', 'Hapus Berhasil');
    else:
      $this->session->set_flashdata('message', 'Hapus Gagal');
    endif;

  redirect(BASE_URL.uriClass);  

}

 // public function do_upload()
 //        {
 //                $config['upload_path']          = '{asset}/uploads/';
 //                $config['allowed_types']        = 'gif|jpg|png';
 //                $config['max_size']             = 100;
 //                $config['max_width']            = 1024;
 //                $config['max_height']           = 768;

 //                $this->load->library('upload', $config);

 //                if ( ! $this->upload->do_upload('img'))
 //                {
 //                        $error = array('error' => $this->upload->display_errors());

 //                        $this->load->view('form', $error);
 //                }
 //                else
 //                {
 //                        $data = array('upload_data' => $this->upload->data());

 //                        $this->load->view('upload_success', $data);
 //                }
 //        }

}
