<!-- Content Wrapper. Contains page content -->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			{title}
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Jadwal Sidang</a></li>
			<li class="active">{title}</li>
		</ol>
	</section>

	<!-- Main content -->
	

	<section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-danger btn-sm" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
              <form role="form" action="{url}panel/jadwal_sidang/{action}" method="post" enctype="multipart/form-data">
              	<input type="hidden" name="id_berita" value="{id_jadwal}">
                <div class="col-md-6">
                  <div class="form-group">
                  <label>Tanggal</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                  </div>
                    <input type="text" class="form-control pull-right" id="datepicker" name="tgl" value="{tgl}">
                  </div>
            	  </div>
              </div>
                <!-- /.input group -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="terdakwa">Terdakwa</label>
                    <input type="terdakwa" class="form-control" id="terdakwa" name="terdakwa" placeholder="Terdakwa" value="{terdakwa}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="jpu">J.P.U</label>
                    <input type="jpu" class="form-control" id="jpu" name="jpu" placeholder="Jaksa Penuntut Umum" value="{jpu}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="hakim">Hakim</label>
                    <input type="hakim" class="form-control" id="hakim" name="hakim" placeholder="Hakim" value="{hakim}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="no_perkara">No. Perkara</label>
                    <input type="no_perkara" class="form-control" id="no_perkara" name="no_perkara" placeholder="No Perkara" value="{no_perkara}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="agenda">Agenda Sidang</label>
                    <input type="agenda" class="form-control" id="agenda" name="agenda" placeholder="Agenda Sidang" value="{agenda}">
                  </div>
                </div>
                <div class="col-md-12">
                <div class="form-group">
                  <label>Kategori Sidang</label>
                  <select name="kat" class="form-control">
                    <option value="PIDSUS">PIDSUS</option>
                    <option value="PIDUM">PIDUM</option>
                    <option value="DATUN">DATUN</option>
                  </select>
                </div><br>
                </div>
                <div class="box-footer">
                	<button type="submit" class="btn btn-primary">Submit</button>
                	<button type="reset" class="btn btn-box-tool">Reset</button>
             	</div>
              </form>
            </div>
          </div>
          <!-- /.box -->

      </div>
      <!-- ./row -->
    </section>

	<!-- /.content -->
</div>
</div>
</body>
<!-- /.content-wrapper -->