<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			{title}
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Data User</a></li>
			<li class="active">{title}</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- Default box -->
		<div class="box box-info">
			<div class="box-header with-border">
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
							title="Collapse">
						<i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
						<i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body">
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{url}panel/user/{action}" method="post">
              <div class="box-body">
              	<input type="hidden" name="id" value="{id}">
 					<div class="input-group">
                        <span class="input-group-addon">@</span>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="{username}">
                    </div>
                    <br>
                    <div class="input-group">
                    	<span class="input-group-addon"><i class="fa fa-key"></i></span>
                        <input type="Password" class="form-control" id="password" name="password" placeholder="Password">
                    </div>
                    <br>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <input type="text" class="form-control" id="email" name="email" placeholder="E-mail" value="{email}">
                    </div>
                    <br>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-group"></i></span>
                        <!-- <input type="text" class="form-control" id="tipe" name="tipe" placeholder="Tipe Admin" value="{tipe}"> -->
                         <select name="tipe" class="form-control">
                    		<option value="superadmin">Superadmin</option>
                    		<option value="admin">Admin</option>
                    		<option value="user">User</option>
                  		</select>
                    </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                 <button type="reset" class="btn btn-box-tool">Reset</button>
              </div>
            </form>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->

	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
