<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_User extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }

    function getData() {
        $return = $this->db->get('user')->result();

        return $return;
    }

    function getDataById($id) {
        $where = array(
            'id' => $id
        );

        $result = $this->db
            ->where($where)
            ->get('user')
            ->result();

        return $result;
    }
}
