<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {


  function __construct()
	{
		parent::__construct();

    $this->load->model('m_user');

    define('uriClass', 'panel/user');
	}

	function index()
	{
    $data = array(
      'user'  => $this->m_user->getData()
    ); 

    $data['url'] = BASE_URL;
		$data['title'] = 'Data User';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'index';

		$this->panel_default->_render_page($data);
  }

  function add()
	{
    $data = array(
      'id'  => '',
      'username'  => '',
      'password'  => '',
      'email'  => '',
      'tipe'  => ''     
    );

    $data['action'] = 'add_save';
    $data['url'] = BASE_URL;
		$data['title'] = 'Tambah User';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'form';
		
    $this->panel_default->_render_page($data);
  }

    function edit()
	{
	  $id = $this->uri->segment(4);

    $dataById = $this->m_user->getDataById($id);

    $data = array(
        'id' => $dataById[0]->id,
        'username' => $dataById[0]->username,
        'password' => $dataById[0]->password,
        'email' => $dataById[0]->email,
        'tipe' => $dataById[0]->tipe
    );

    $data['action'] = 'edit_save';
    $data['url'] = BASE_URL;
  	$data['title'] = 'Dashboard | Edit';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'form';
		$this->panel_default->_render_page($data);
  



  }

function add_save()
{
    $data = array(
      'id'  => $this->input->post('id',TRUE),
      'username'  => $this->input->post('username',TRUE),
      'password'  => md5($this->input->post('password')),
      'email'  => $this->input->post('email',TRUE),
      'tipe'  => $this->input->post('tipe',TRUE)      
    );

    $insert = $this->db->insert('user', $data);

    if($insert == TRUE):
      $this->session->set_flashdata('message', 'Input Berhasil');
    else:
      $this->session->set_flashdata('message', 'Input Gagal');
    endif;

    redirect(BASE_URL.uriClass);  
}

function edit_save()
{
    $where = array(
      'id'  => $this->input->post('id',TRUE)
    );

    $data = array(
      'username'  => $this->input->post('username',TRUE),
      'password'  => md5($this->input->post('password')),
      'email'  => $this->input->post('email',TRUE),
      'tipe'  => $this->input->post('tipe',TRUE)       
    );

    $update = $this->db
    ->where($where)
    ->update('user', $data);

    if($update == TRUE):
      $this->session->set_flashdata('message', 'Edit Berhasil');
    else:
      $this->session->set_flashdata('message', 'Edit Gagal');
    endif;

    redirect(BASE_URL.uriClass);  

}

function delete()
{
  $id = $this->uri->segment(4);
  $where = array(
      'id'  => $id
  );

  $delete = $this->db
    ->where($where)
    ->delete('user');

  if($delete == TRUE):
      $this->session->set_flashdata('message', 'Hapus Berhasil');
    else:
      $this->session->set_flashdata('message', 'Hapus Gagal');
    endif;

  redirect(BASE_URL.uriClass);  

}


}
