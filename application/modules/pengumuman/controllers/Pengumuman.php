<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pengumuman extends CI_Controller {


  function __construct()
	{
		parent::__construct();

    $this->load->model('M_Pengumuman');

    define('uriClass', 'panel/berita');
    $this->load->helper(array('form', 'url'));
	}

	function index()
	{
    $data = array(
      'pengumuman'  => $this->M_Pengumuman->getData()
    ); 

    $data['url'] = BASE_URL;
		$data['title'] = 'Pengumuman';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'index';

		$this->panel_default->_render_page($data);
  }

  function add()
	{
    $data = array(
      'id_png'  => '',
      'judul'  => '',
      'isi'  => '',
      'penulis'  => '',
      'tgl'  => ''      
    );

    $data['action'] = 'add_save';
    $data['url'] = BASE_URL;
		$data['title'] = 'Tambah Pengumuman';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'form';
		
    $this->panel_default->_render_page($data);
  }

    function edit()
	{
	  $id_png = $this->uri->segment(4);

    $dataById = $this->M_Pengumuman->getDataById($id_png);

    $data = array(
        'id_png' => $dataById[0]->id_png,
        'judul' => $dataById[0]->judul,
        'isi' => $dataById[0]->isi,
        'penulis' => $dataById[0]->penulis,
        'tgl' => $dataById[0]->tgl
    );

    $data['action'] = 'edit_save';
    $data['url'] = BASE_URL;
  	$data['title'] = 'Dashboard | Edit';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'form';
		$this->panel_default->_render_page($data);
  



  }

function add_save()
{
    $tgl = $this->input->post('tgl',TRUE) ?: date('Y-m-d');

    $data = array(
      'id_png'  => $this->input->post('id_png',TRUE),
      'judul'  => $this->input->post('judul',TRUE),
      'isi'  => $this->input->post('isi',TRUE),
      'penulis'  => $this->input->post('penulis',TRUE),
      'tgl'  => $tgl     
    );

    $insert = $this->db->insert('pengumuman', $data);

    if($insert == TRUE):
      $this->session->set_flashdata('message', 'Input Berhasil');
    else:
      $this->session->set_flashdata('message', 'Input Gagal');
    endif;

    redirect(BASE_URL.uriClass);  
}

function edit_save()
{
    $where = array(
      'id_png'  => $this->input->post('id_png',TRUE)
    );

    $data = array(
      'judul'  => $this->input->post('judul',TRUE),
      'isi'  => $this->input->post('isi',TRUE),
      'penulis'  => $this->input->post('penulis',TRUE),
      'tgl'  => $this->input->post('tgl',TRUE)      
    );

    $update = $this->db
    ->where($where)
    ->update('pengumuman', $data);

    if($update == TRUE):
      $this->session->set_flashdata('message', 'Edit Berhasil');
    else:
      $this->session->set_flashdata('message', 'Edit Gagal');
    endif;

    redirect(BASE_URL.uriClass);  

}

function delete()
{
  $id_png = $this->uri->segment(4);
  $where = array(
      'id_png'  => $id_png
  );

  $delete = $this->db
    ->where($where)
    ->delete('pengumuman');

  if($delete == TRUE):
      $this->session->set_flashdata('message', 'Hapus Berhasil');
    else:
      $this->session->set_flashdata('message', 'Hapus Gagal');
    endif;

  redirect(BASE_URL.uriClass);  

}

 // public function do_upload()
 //        {
 //                $config['upload_path']          = '{asset}/uploads/';
 //                $config['allowed_types']        = 'gif|jpg|png';
 //                $config['max_size']             = 100;
 //                $config['max_width']            = 1024;
 //                $config['max_height']           = 768;

 //                $this->load->library('upload', $config);

 //                if ( ! $this->upload->do_upload('img'))
 //                {
 //                        $error = array('error' => $this->upload->display_errors());

 //                        $this->load->view('form', $error);
 //                }
 //                else
 //                {
 //                        $data = array('upload_data' => $this->upload->data());

 //                        $this->load->view('upload_success', $data);
 //                }
 //        }

}
