<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Pengumuman extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }

    function getData() {
        $return = $this->db->get('pengumuman')->result();

        return $return;
    }

    function getDataById($id_png) {
        $where = array(
            'id_png' => $id_png
        );

        $result = $this->db
            ->where($where)
            ->get('pengumuman')
            ->result();

        return $result;
    }
}
