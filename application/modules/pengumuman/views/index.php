<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {title}
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> {title}</a></li>
        </ol>
        <?php 
        if ($this->session->flashdata('message')) {
        ?>
            <br>
            <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> <?=$this->session->flashdata('message')?></h4>
            </div>
        <?php  
        }
        ?>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box box-info">
            <div class="box-header with-border">
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            <div class="row">
                <div class="col-md-2">
                    <a href="{url}panel/pengumuman/add" type="button" class="btn bg-orange btn-flat margin">+ Tambah Data</a>
                </div>
            </div>
            </div>
            
            <div class="box-body">
            <!-- /.box-header -->
            <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <tr>
                  <th>NO</th>
                  <th>JUDUL</th>
                  <th>PENULIS</th>
                  <th>TANGGAL</th>
                  <th>ISI</th>
                  <th colspan="2" class="text-center"><span class="glyphicon glyphicon-cog"></span></th>
                </tr>
                <?php
                    $no = 1;
                foreach($pengumuman as $data){
                ?>
                  <tr>
                    <td class="align-middle"><?php echo $no; ?></td>
                    <td class="align-middle"><?php echo $data->judul; ?></td>
                    <td class="align-middle"><?php echo $data->penulis; ?></td>
                    <td class="align-middle"><?php echo $data->tgl; ?></td>
                    <td class="align-middle"><?php echo $data->isi; ?></td>
                    <td class="align-middle text-center">
                      <a href="{url}panel/pengumuman/edit/<?php echo $data->id_png; ?>" class="btn btn-info btn-form-ubah"><i class="glyphicon glyphicon-pencil"></i></a></td>

                                <!-- Membuat sebuah textbox hidden yang akan digunakan untuk form ubah -->
                                <input type="hidden" class="judul-value" value="<?php echo $data->judul; ?>">
                                <input type="hidden" class="penulis-value" value="<?php echo $data->penulis; ?>">
                                <input type="hidden" class="tgl-value" value="<?php echo $data->tgl; ?>">
                                <input type="hidden" class="isi-value" value="<?php echo $data->isi; ?>">
                    <td>
                      <a href="{url}panel/pengumuman/delete/<?php echo $data->id_png; ?>" class="btn btn-danger btn-alert-hapus" onclick="return confirm('Apakah anda yakin akan menghapus data ini?'"><i class="glyphicon glyphicon-erase"></i></a></td>
                    </td>
                  </tr>
                <?php
                  $no++; // Tambah 1 setiap kali looping
                }
                ?>
              </table>
            </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
