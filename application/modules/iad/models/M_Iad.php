<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Iad extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }

    function getData() {
        $return = $this->db->get('iad')->result();

        return $return;
    }

    function getDataById($id_iad) {
        $where = array(
            'id_iad' => $id_iad
        );

        $result = $this->db
            ->where($where)
            ->get('iad')
            ->result();

        return $result;
    }
}
