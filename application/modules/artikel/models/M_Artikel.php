<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Artikel extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }

    function getData() {
        $return = $this->db->get('artikel')->result();

        return $return;
    }

    function getDataById($id_artikel) {
        $where = array(
            'id_artikel' => $id_artikel
        );

        $result = $this->db
            ->where($where)
            ->get('artikel')
            ->result();

        return $result;
    }
}
