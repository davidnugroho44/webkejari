<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Berita extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }

    function getData() {
        $return = $this->db->get('berita')->result();

        return $return;
    }

    function getDataById($id_berita) {
        $where = array(
            'id_berita' => $id_berita
        );

        $result = $this->db
            ->where($where)
            ->get('berita')
            ->result();

        return $result;
    }
}
