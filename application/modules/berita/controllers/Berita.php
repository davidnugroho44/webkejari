<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller {


  function __construct()
	{
		parent::__construct();

    $this->load->model('M_Berita');

    define('uriClass', 'panel/berita');
    $this->load->helper(array('form', 'url'));
    $this->load->library('upload');
    $this->load->dbforge();
	}

	function index()
	{
    $data = array(
      'berita'  => $this->M_Berita->getData()
    ); 

    $data['url'] = BASE_URL;
		$data['title'] = 'Berita';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'index';

		$this->panel_default->_render_page($data);
  }

  function add()
	{
    $data = array(
      'id_berita'  => '',
      'judul'  => '',
      'img'  => '',
      'isi'  => '',
      'tgl'  => ''      
    );

    $data['action'] = 'add_save';
    $data['url'] = BASE_URL;
		$data['title'] = 'Tambah Berita';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'form';
		
    $this->panel_default->_render_page($data);
  }

    function edit()
	{
	  $id_berita = $this->uri->segment(4);

    $dataById = $this->M_Berita->getDataById($id_berita);

    $data = array(
        'id_berita' => $dataById[0]->id_berita,
        'judul' => $dataById[0]->judul,
        'img' => $dataById[0]->img,
        'isi' => $dataById[0]->isi,
        'tgl' => $dataById[0]->tgl
    );

    $data['action'] = 'edit_save';
    $data['url'] = BASE_URL;
  	$data['title'] = 'Dashboard | Edit';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'formEdit';
		$this->panel_default->_render_page($data);
  



  }

  function add_save()
  {
      $tgl = $this->input->post('tgl',TRUE) ?: date('Y-m-d');

      $data = array(
        'id_berita'  => $this->input->post('id_berita',TRUE),
        'judul'  => $this->input->post('judul',TRUE),
        'img'  => $this->upload_file('img','jpg|jpeg|png|gif'),
        'isi'  => $this->input->post('isi',TRUE),
        'tgl'  => $tgl     
      );

      $insert = $this->db->insert('berita', $data);

      if($insert == TRUE):
        $this->session->set_flashdata('message', 'Input Berhasil');
      else:
        $this->session->set_flashdata('message', 'Input Gagal');
      endif;

      redirect(BASE_URL.uriClass);  
  }

  function edit_save()
  {
      $where = array(
        'id_berita'  => $this->input->post('id_berita',TRUE)
      );

      $data = array(
        'judul'  => $this->input->post('judul',TRUE),
        'img'  => $this->upload_file('img','jpg|jpeg|png|gif'),
        'isi'  => $this->input->post('isi',TRUE),
        'tgl'  => $this->input->post('tgl',TRUE)      
      );

      $update = $this->db
      ->where($where)
      ->update('berita', $data);

      if($update == TRUE):
        $this->session->set_flashdata('message', 'Edit Berhasil');
      else:
        $this->session->set_flashdata('message', 'Edit Gagal');
      endif;

      redirect(BASE_URL.uriClass);  

  }

  function delete()
  {
    $id_berita = $this->uri->segment(4);
    $where = array(
        'id_berita'  => $id_berita
    );

    $delete = $this->db
      ->where($where)
      ->delete('berita');

    if($delete == TRUE):
        $this->session->set_flashdata('message', 'Hapus Berhasil');
      else:
        $this->session->set_flashdata('message', 'Hapus Gagal');
      endif;

    redirect(BASE_URL.uriClass);  

  }

    public function upload_file($attribute, $extension)
    {
        $fileName = $this->input->post($attribute);
        $config['upload_path'] = './upload/';
        $config['file_name'] = $fileName;
        $config['allowed_types'] = $extension;
        $config['max_size'] = 10000;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (!$this->upload->do_upload($attribute)) {
            return $this->upload->display_errors();
        } else {
            return $this->upload->data()['file_name'];
        }
    }

 // public function do_upload()
 //        {
 //                $config['upload_path']          = '{asset}/uploads/';
 //                $config['allowed_types']        = 'gif|jpg|png';
 //                $config['max_size']             = 100;
 //                $config['max_width']            = 1024;
 //                $config['max_height']           = 768;

 //                $this->load->library('upload', $config);

 //                if ( ! $this->upload->do_upload('img'))
 //                {
 //                        $error = array('error' => $this->upload->display_errors());

 //                        $this->load->view('form', $error);
 //                }
 //                else
 //                {
 //                        $data = array('upload_data' => $this->upload->data());

 //                        $this->load->view('upload_success', $data);
 //                }
 //        }

}
