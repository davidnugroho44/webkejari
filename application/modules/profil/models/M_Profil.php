<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Profil extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }

    function getData() {
        $return = $this->db->get('profil')->result();

        return $return;
    }

    function getDataById($id_profil) {
        $where = array(
            'id_profil' => $id_profil
        );

        $result = $this->db
            ->where($where)
            ->get('profil')
            ->result();

        return $result;
    }
}
