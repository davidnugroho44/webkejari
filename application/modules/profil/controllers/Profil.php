<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {


  function __construct()
	{
		parent::__construct();

    $this->load->model('M_Profil');

    define('uriClass', 'panel/profil');
    $this->load->library('upload');
    $this->load->dbforge();
  
	}

	function index()
	{
    $data = array(
      'profil'  => $this->M_Profil->getData()
    ); 

    $data['url'] = BASE_URL;
		$data['title'] = 'Profil';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'index';

		$this->panel_default->_render_page($data);
  }

  function add()
	{
    $data = array(
      'id_profil'  => '',
      'judul'  => '',
      'img'  => '',
      'isi'  => ''     
    );

    $data['action'] = 'add_save';
    $data['url'] = BASE_URL;
		$data['title'] = 'Tambah Profil';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'form';
		
    $this->panel_default->_render_page($data);
  }

    function edit()
	{
	  $id_profil = $this->uri->segment(4);

    $dataById = $this->M_Profil->getDataById($id_profil);

    $data = array(
        'id_profil' => $dataById[0]->id_profil,
        'judul' => $dataById[0]->judul,
        'img' => $dataById[0]->img,
        'isi' => $dataById[0]->isi
    );

    $data['action'] = 'edit_save';
    $data['url'] = BASE_URL;
  	$data['title'] = 'Dashboard | Edit';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'formEdit';
		$this->panel_default->_render_page($data);
  



  }

    function add_save()
    {
        $data = array(
          'id_profil'  => $this->input->post('id_profil',TRUE),
          'judul'  => $this->input->post('judul',TRUE),
          'img'  => $this->upload_file('img','jpg|jpeg|png|gif'),
          'isi'  => $this->input->post('isi',TRUE)     
        );

        $insert = $this->db->insert('profil', $data);

        if($insert == TRUE):
          $this->session->set_flashdata('message', 'Input Berhasil');
        else:
          $this->session->set_flashdata('message', 'Input Gagal');
        endif;

        redirect(BASE_URL.uriClass);  
    }

    function edit_save()
    {
        $where = array(
          'id_profil'  => $this->input->post('id_profil',TRUE)
        );

        $data = array(
          'judul'  => $this->input->post('judul',TRUE),
          'img'  => $this->upload_file('img','jpg|jpeg|png|gif'),
          'isi'  => $this->input->post('isi',TRUE)     
        );

        $update = $this->db
        ->where($where)
        ->update('profil', $data);

        if($update == TRUE):
          $this->session->set_flashdata('message', 'Edit Berhasil');
        else:
          $this->session->set_flashdata('message', 'Edit Gagal');
        endif;

        redirect(BASE_URL.uriClass);  

    }

    function delete()
    {
      $id_profil = $this->uri->segment(4);
      $where = array(
          'id_profil'  => $id_profil
      );

      $delete = $this->db
        ->where($where)
        ->delete('profil');

      if($delete == TRUE):
          $this->session->set_flashdata('message', 'Hapus Berhasil');
        else:
          $this->session->set_flashdata('message', 'Hapus Gagal');
        endif;

      redirect(BASE_URL.uriClass);  

    }

    public function upload_file($attribute, $extension)
    {
        $fileName = $this->input->post($attribute);
        $config['upload_path'] = './upload/';
        $config['file_name'] = $fileName;
        $config['allowed_types'] = $extension;
        $config['max_size'] = 10000;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (!$this->upload->do_upload($attribute)) {
            return $this->upload->display_errors();
        } else {
            return $this->upload->data()['file_name'];
        }
    }

}
