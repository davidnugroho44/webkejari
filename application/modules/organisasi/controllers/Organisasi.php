<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Organisasi extends CI_Controller {


  function __construct()
	{
		parent::__construct();

    $this->load->model('M_Organisasi');

    define('uriClass', 'panel/organisasi');
	}

	function index()
	{
    $data = array(
      'organisasi'  => $this->M_Organisasi->getData()
    ); 

    $data['url'] = BASE_URL;
		$data['title'] = 'Organisasi';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'index';

		$this->panel_default->_render_page($data);
  }

  function add()
	{
    $data = array(
      'id_org'  => '',
      'judul'  => '',
      'img'  => '',
      'isi'  => ''     
    );

    $data['action'] = 'add_save';
    $data['url'] = BASE_URL;
		$data['title'] = 'Tambah Organisasi';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'form';
		
    $this->panel_default->_render_page($data);
  }

    function edit()
	{
	  $id_org = $this->uri->segment(4);

    $dataById = $this->M_Organisasi->getDataById($id_org);

    $data = array(
        'id_org' => $dataById[0]->id_org,
        'judul' => $dataById[0]->judul,
        'img' => $dataById[0]->img,
        'isi' => $dataById[0]->isi
    );

    $data['action'] = 'edit_save';
    $data['url'] = BASE_URL;
  	$data['title'] = 'Dashboard | Edit';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'form';
		$this->panel_default->_render_page($data);
  



  }

function add_save()
{
    $data = array(
      'id_org'  => $this->input->post('id_org',TRUE),
      'judul'  => $this->input->post('judul',TRUE),
      'img'  => $this->input->post('img',TRUE),
      'isi'  => $this->input->post('isi',TRUE)     
    );

    $insert = $this->db->insert('organisasi', $data);

    if($insert == TRUE):
      $this->session->set_flashdata('message', 'Input Berhasil');
    else:
      $this->session->set_flashdata('message', 'Input Gagal');
    endif;

    redirect(BASE_URL.uriClass);  
}

function edit_save()
{
    $where = array(
      'id_org'  => $this->input->post('id_org',TRUE)
    );

    $data = array(
      'judul'  => $this->input->post('judul',TRUE),
      'img'  => $this->input->post('img',TRUE),
      'isi'  => $this->input->post('isi',TRUE)     
    );

    $update = $this->db
    ->where($where)
    ->update('organisasi', $data);

    if($update == TRUE):
      $this->session->set_flashdata('message', 'Edit Berhasil');
    else:
      $this->session->set_flashdata('message', 'Edit Gagal');
    endif;

    redirect(BASE_URL.uriClass);  

}

function delete()
{
  $id_org = $this->uri->segment(4);
  $where = array(
      'id_org'  => $id_org
  );

  $delete = $this->db
    ->where($where)
    ->delete('organisasi');

  if($delete == TRUE):
      $this->session->set_flashdata('message', 'Hapus Berhasil');
    else:
      $this->session->set_flashdata('message', 'Hapus Gagal');
    endif;

  redirect(BASE_URL.uriClass);  

}


}
