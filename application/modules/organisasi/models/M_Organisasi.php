<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Organisasi extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }

    function getData() {
        $return = $this->db->get('organisasi')->result();

        return $return;
    }

    function getDataById($id_org) {
        $where = array(
            'id_org' => $id_org
        );

        $result = $this->db
            ->where($where)
            ->get('organisasi')
            ->result();

        return $result;
    }
}
