<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Pidum extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }

    function getData() {
        $return = $this->db->get('pidum')->result();

        return $return;
    }

    function getDataById($id_pidum) {
        $where = array(
            'id_pidum' => $id_pidum
        );

        $result = $this->db
            ->where($where)
            ->get('pidum')
            ->result();

        return $result;
    }
}
