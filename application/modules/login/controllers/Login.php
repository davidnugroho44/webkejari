<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {


  function __construct()
	{
		parent::__construct();

    $this->load->model('m_login');

    define('uriClass', 'panel/login');
	}

	function index()
	{
    // $data = array(
    //   'login'  => $this->m_login->getData()
    // ); 

    $data['url'] = BASE_URL;
		$data['title'] = 'Login Administrator';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'index';
    $data['action'] = 'login_process';

		$this->panel_default->_render_default($data);
  }

  function login_process() {
    // $email = $this->input->post('email');
    $username = $this->input->post('username');
    $password = md5($this->input->post('password'));

    $login = $this->m_login->get($username, $password);

    if (count($login) > 0) :
      $data_session = array(
        'email'     => $login[0]->email,
        'username'  => $login[0]->username,
      );

      $this->session->set_userdata($data_session);

      redirect(BASE_URL('panel'));
    else :
      $this->session->set_flashdata('message', 'Login Gagal');
      redirect(BASE_URL('panel/login'));  
    endif;

    array_print(count($login));
  }

  public function logout(){
    $this->session->sess_destroy();

    $this->session->set_flashdata('message', 'Logout Berhasil!');
    redirect(BASE_URL('panel/login'));    
  }

}
