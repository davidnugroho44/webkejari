<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Login extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }

    function get($username, $password){
        $where = array(
            'username' => $username,
            'password' => $password
        );

        $result = $this->db
            ->where($where)
            ->get('user')
            ->result();

        return $result; // Untuk menambahkan Where Clause : username='$username'
    }
}
