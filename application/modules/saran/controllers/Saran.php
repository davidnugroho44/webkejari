<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Saran extends CI_Controller {


  function __construct()
	{
		parent::__construct();

    $this->load->model('m_saran');

    define('uriClass', 'panel/saran');
	}

	function index()
	{
    $data = array(
      'saran'  => $this->m_saran->getData()
    ); 

    $data['url'] = BASE_URL;
		$data['title'] = 'Data Saran';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'index';

		$this->panel_default->_render_page($data);
  }

  function add()
	{
    $data = array(
      'id_saran'  => '',
      'nama'  => '',
      'alamat'  => '',
      'no_hp'  => '',
      'pesan'  => '',
      'tgl'  => ''     
    );

    $data['action'] = 'add_save';
    $data['url'] = BASE_URL;
		$data['title'] = 'Tambah Saran';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'form';
		
    $this->panel_default->_render_page($data);
  }

    function edit()
	{
	  $id_saran = $this->uri->segment(4);

    $dataById = $this->m_saran->getDataById($id);

    $data = array(
        'id_saran' => $dataById[0]->id_saran,
        'nama' => $dataById[0]->nama,
        'alamat' => $dataById[0]->alamat,
        'no_hp' => $dataById[0]->email,
        'pesan' => $dataById[0]->pesan,
        'tgl' => $dataById[0]->tgl
    );

    $data['action'] = 'edit_save';
    $data['url'] = BASE_URL;
  	$data['title'] = 'Dashboard | Edit';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'form';
		$this->panel_default->_render_page($data);

  }

function add_save()
{
    $data = array(
      'id_saran'  => $this->input->post('id_saran',TRUE),
      'nama'  => $this->input->post('nama',TRUE),
      'alamat'  => md5($this->input->post('alamat')),
      'no_hp'  => $this->input->post('no_hp',TRUE),
      'pesan'  => $this->input->post('pesan',TRUE),
      'tgl'  => $this->input->post('tgl',TRUE)      
    );

    $insert = $this->db->insert('saran', $data);

    if($insert == TRUE):
      $this->session->set_flashdata('message', 'Input Berhasil');
    else:
      $this->session->set_flashdata('message', 'Input Gagal');
    endif;

    redirect(BASE_URL.uriClass);  
}

function edit_save()
{
    $where = array(
      'id_saran'  => $this->input->post('id_saran',TRUE)
    );

    $data = array(
      'id_saran'  => $this->input->post('id_saran',TRUE),
      'nama'  => $this->input->post('nama',TRUE),
      'alamat'  => md5($this->input->post('alamat')),
      'no_hp'  => $this->input->post('no_hp',TRUE),
      'pesan'  => $this->input->post('pesan',TRUE),
      'tgl'  => $this->input->post('tgl',TRUE)      
    );

    $update = $this->db
    ->where($where)
    ->update('saran', $data);

    if($update == TRUE):
      $this->session->set_flashdata('message', 'Edit Berhasil');
    else:
      $this->session->set_flashdata('message', 'Edit Gagal');
    endif;

    redirect(BASE_URL.uriClass);  

}

function delete()
{
  $id_saran = $this->uri->segment(4);
  $where = array(
      'id_saran'  => $id_saran
  );

  $delete = $this->db
    ->where($where)
    ->delete('saran');

  if($delete == TRUE):
      $this->session->set_flashdata('message', 'Hapus Berhasil');
    else:
      $this->session->set_flashdata('message', 'Hapus Gagal');
    endif;

  redirect(BASE_URL.uriClass);  

}


}
