<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Saran extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }

    function getData() {
        $return = $this->db->get('saran')->result();

        return $return;
    }

    function getDataById($id_saran) {
        $where = array(
            'id_saran' => $id_saran
        );

        $result = $this->db
            ->where($where)
            ->get('saran')
            ->result();

        return $result;
    }
}
