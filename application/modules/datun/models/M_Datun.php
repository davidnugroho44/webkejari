<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Datun extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }

    function getData() {
        $return = $this->db->get('datun')->result();

        return $return;
    }

    function getDataById($id_datun) {
        $where = array(
            'id_datun' => $id_datun
        );

        $result = $this->db
            ->where($where)
            ->get('datun')
            ->result();

        return $result;
    }
}
