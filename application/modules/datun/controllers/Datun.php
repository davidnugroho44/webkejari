<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Datun extends CI_Controller {


  function __construct()
	{
		parent::__construct();

    $this->load->model('M_Datun');

    define('uriClass', 'panel/datun');
    $this->load->helper(array('form', 'url'));
	}

	function index()
	{
    $data = array(
      'datun'  => $this->M_Datun->getData()
    ); 

    $data['url'] = BASE_URL;
		$data['title'] = 'Info Perdata dan Tata Usaha Negara';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'index';

		$this->panel_default->_render_page($data);
  }

  function add()
	{
    $data = array(
      'id_datun'  => '',
      'no_perkara'  => '',
      'nama'  => '',
      'identitas'  => '',
      'status'  => ''    
    );

    $data['action'] = 'add_save';
    $data['url'] = BASE_URL;
		$data['title'] = 'Tambah Info Perdata dan Tata Usaha Negara';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'form';
		
    $this->panel_default->_render_page($data);
  }

    function edit()
	{
	  $id_datun = $this->uri->segment(4);

    $dataById = $this->M_Datun->getDataById($id_datun);

    $data = array(
        'id_datun' => $dataById[0]->id_datun,
        'no_perkara' => $dataById[0]->no_perkara,
        'nama' => $dataById[0]->nama,
        'identitas' => $dataById[0]->identitas,
        'status' => $dataById[0]->status
    );

    $data['action'] = 'edit_save';
    $data['url'] = BASE_URL;
  	$data['title'] = 'Dashboard | Edit';
		$data['asset'] = $this->panel_default->asset_admin();
		$data['viewspage'] = 'form';
		$this->panel_default->_render_page($data);
  



  }

function add_save()
{
    $data = array(
      'id_datun'  => $this->input->post('id_datun',TRUE),
      'no_perkara'  => $this->input->post('no_perkara',TRUE),
      'nama'  => $this->input->post('nama',TRUE),
      'identitas'  => $this->input->post('identitas',TRUE),
      'status'  => $this->input->post('status',TRUE)      
    );

    $insert = $this->db->insert('datun', $data);

    if($insert == TRUE):
      $this->session->set_flashdata('message', 'Input Berhasil');
    else:
      $this->session->set_flashdata('message', 'Input Gagal');
    endif;

    redirect(BASE_URL.uriClass);  
}

function edit_save()
{
    $where = array(
      'id_datun'  => $this->input->post('id_datun',TRUE)
    );

    $data = array(
      'id_datun'  => $this->input->post('id_datun',TRUE),
      'no_perkara'  => $this->input->post('no_perkara',TRUE),
      'nama'  => $this->input->post('nama',TRUE),
      'identitas'  => $this->input->post('identitas',TRUE),
      'status'  => $this->input->post('status',TRUE)         
    );

    $update = $this->db
    ->where($where)
    ->update('datun', $data);

    if($update == TRUE):
      $this->session->set_flashdata('message', 'Edit Berhasil');
    else:
      $this->session->set_flashdata('message', 'Edit Gagal');
    endif;

    redirect(BASE_URL.uriClass);  

}

function delete()
{
  $id_datun = $this->uri->segment(4);
  $where = array(
      'id_datun'  => $id_datun
  );

  $delete = $this->db
    ->where($where)
    ->delete('datun');

  if($delete == TRUE):
      $this->session->set_flashdata('message', 'Hapus Berhasil');
    else:
      $this->session->set_flashdata('message', 'Hapus Gagal');
    endif;

  redirect(BASE_URL.uriClass);  

}

 // public function do_upload()
 //        {
 //                $config['upload_path']          = '{asset}/uploads/';
 //                $config['allowed_types']        = 'gif|jpg|png';
 //                $config['max_size']             = 100;
 //                $config['max_width']            = 1024;
 //                $config['max_height']           = 768;

 //                $this->load->library('upload', $config);

 //                if ( ! $this->upload->do_upload('img'))
 //                {
 //                        $error = array('error' => $this->upload->display_errors());

 //                        $this->load->view('form', $error);
 //                }
 //                else
 //                {
 //                        $data = array('upload_data' => $this->upload->data());

 //                        $this->load->view('upload_success', $data);
 //                }
 //        }

}
