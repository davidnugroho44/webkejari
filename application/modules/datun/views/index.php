<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {title}
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> {title}</a></li>
        </ol>
        <?php 
        if ($this->session->flashdata('message')) {
        ?>
            <br>
            <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> <?=$this->session->flashdata('message')?></h4>
            </div>
        <?php  
        }
        ?>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box box-info">
            <div class="box-header with-border">
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            <div class="row">
                <div class="col-md-2">
                    <a href="{url}panel/datun/add" type="button" class="btn bg-orange btn-flat margin">+ Tambah Data</a>
                </div>
            </div>
            </div>
            
            <div class="box-body">
            <!-- /.box-header -->
            <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <tr>
                  <th>No</th>
                  <th>No. Perkara</th>
                  <th>Nama Perkara</th>
                  <th>Identitas Tersangka</th>
                  <th>Status</th>
                  <th colspan="2" class="text-center"><span class="glyphicon glyphicon-cog"></span></th>
                </tr>
                <?php
                    $no = 1;
                foreach($datun as $data){
                ?>
                  <tr>
                    <td class="align-middle"><?php echo $no; ?></td>
                    <td class="align-middle"><?php echo $data->no_perkara; ?></td>
                    <td class="align-middle"><?php echo $data->nama; ?></td>
                    <td class="align-middle"><?php echo $data->identitas; ?></td>
                    <td class="align-middle"><?php echo $data->status; ?></td>
                    <td class="align-middle text-center">
                      <a href="{url}panel/datun/edit/<?php echo $data->id_datun; ?>" class="btn btn-info btn-form-ubah"><span class="glyphicon glyphicon-pencil"></span></a></td>

                                <!-- Membuat sebuah textbox hidden yang akan digunakan untuk form ubah -->
                                <input type="hidden" class="no_perkara-value" value="<?php echo $data->no_perkara; ?>">
                                <input type="hidden" class="nama-value" value="<?php echo $data->nama; ?>">
                                <input type="hidden" class="identitas-value" value="<?php echo $data->identitas; ?>">
                                <input type="hidden" class="status-value" value="<?php echo $data->status; ?>">
                    <td>
                      <a href="{url}panel/datun/delete/<?php echo $data->id_datun; ?>" class="btn btn-danger btn-alert-hapus" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><span class="glyphicon glyphicon-erase"></span></a></td>
                    </td>
                  </tr>
                <?php
                  $no++; // Tambah 1 setiap kali looping
                }
                ?>
              </table>
            </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
