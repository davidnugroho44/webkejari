<!-- Content Wrapper. Contains page content -->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			{title}
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Pidana Khusus</a></li>
			<li class="active">{title}</li>
		</ol>
	</section>

	<!-- Main content -->
	

	<section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-danger btn-sm" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
              <form role="form" action="{url}panel/pidsus/{action}" method="post" enctype="multipart/form-data">
              	<input type="hidden" name="id_pidsus" value="{id_pidsus}">
                <!-- /.input group -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="no_perkara">No. Perkara</label>
                    <input type="no_perkara" class="form-control" id="no_perkara" name="no_perkara" placeholder="Nomor Perkara" value="{no_perkara}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="nama">Nama Perkara</label>
                    <input type="nama" class="form-control" id="nama" name="nama" placeholder="Nama" value="{nama}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                  <label>Status</label>
                  <select name="status" class="form-control">
                    <option value="PENUNTUTAN">PENUNTUTAN</option>
                    <option value="EKSEKUSI">EKSEKUSI</option>
                  </select>
                </div><br>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="identitas">Identitas Tersangka</label>
                     <textarea class="form-control" name="identitas">{identitas}</textarea>
                  </div>
                </div>
                <div class="box-footer">
                	<button type="submit" class="btn btn-primary">Submit</button>
                	<button type="reset" class="btn btn-box-tool">Reset</button>
             	</div>
              </form>
            </div>
          </div>
          <!-- /.box -->

      </div>
      <!-- ./row -->
    </section>

	<!-- /.content -->
</div>
</div>
</body>
<!-- /.content-wrapper -->