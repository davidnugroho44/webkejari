<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Pidsus extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }

    function getData() {
        $return = $this->db->get('pidsus')->result();

        return $return;
    }

    function getDataById($id_pidsus) {
        $where = array(
            'id_pidsus' => $id_pidsus
        );

        $result = $this->db
            ->where($where)
            ->get('pidsus')
            ->result();

        return $result;
    }
}
