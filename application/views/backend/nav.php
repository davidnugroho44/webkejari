
  <header class="main-header">
    <!-- Logo -->
    <a href="{url}panel/" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>K</b>S3</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>KejariSalatiga</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{asset}/dist/img/logo_ks.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?=$this->session->userdata['username'];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{asset}/dist/img/logo_ks.png" class="img-circle" alt="User Image">

                <p>
                  <?=$this->session->userdata['username'];?>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a href="<?= BASE_URL('panel/login/logout')?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{asset}/dist/img/logo_ks.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?=$this->session->userdata['username'];?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="{url}panel/">
            <i class="fa fa-home"></i> <span>Home</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-institution"></i> <span>Berita</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{url}panel/berita"><i class="fa fa-circle-o"></i> Berita</a></li>
            <li><a href="{url}panel/berita/add"><i class="fa fa-circle-o"></i> Tambah Data</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-institution"></i> <span>Sarana</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{url}panel/sarana"><i class="fa fa-circle-o"></i> Sarana</a></li>
            <li><a href="{url}panel/sarana/add"><i class="fa fa-circle-o"></i> Tambah Data</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-institution"></i> <span>Artikel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{url}panel/artikel"><i class="fa fa-circle-o"></i> Artikel</a></li>
            <li><a href="{url}panel/artikel/add"><i class="fa fa-circle-o"></i> Tambah Data</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-institution"></i> <span>Pengumuman</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{url}panel/pengumuman"><i class="fa fa-circle-o"></i> Data Pengumuman</a></li>
            <li><a href="{url}panel/pengumuman/add"><i class="fa fa-circle-o"></i> Tambah Data</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-institution"></i> <span>Jadwal Sidang</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{url}panel/jadwal_sidang"><i class="fa fa-circle-o"></i> Data Jadwal Sidang</a></li>
            <li><a href="{url}panel/jadwal_sidang/add"><i class="fa fa-circle-o"></i> Tambah Data</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-institution"></i> <span>Profil</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{url}panel/profil"><i class="fa fa-circle-o"></i> Data Profil</a></li>
            <li><a href="{url}panel/profil/add"><i class="fa fa-circle-o"></i> Tambah Data</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-institution"></i> <span>I.A.D</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{url}panel/iad"><i class="fa fa-circle-o"></i> Data I.A.D</a></li>
            <li><a href="{url}panel/iad/add"><i class="fa fa-circle-o"></i> Tambah Data</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-institution"></i> <span>Info Perkara</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{url}panel/pidum"><i class="fa fa-circle-o"></i> Pidana Umum</a></li>
            <li><a href="{url}panel/pidsus"><i class="fa fa-circle-o"></i> Pidana Khusus</a></li>
            <li><a href="{url}panel/datun"><i class="fa fa-circle-o"></i> Perdata dan Tata Usaha Negara</a></li>
          </ul>
        </li>
<!--        <li class="treeview">
          <a href="{url}panel/saran">
            <i class="fa fa-institution"></i> <span>Kritik dan Saran</span>
          </a>
       </li> -->
       <li><a href="{url}panel/saran"><i class="fa fa-circle-o"></i> Kritik dan Saran</a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>User</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{url}panel/user"><i class="fa fa-circle-o"></i> Data User</a></li>
            <li><a href="{url}panel/user/add"><i class="fa fa-circle-o"></i> Tambah Data</a></li>
            <li><a href="{url}panel/saran"><i class="fa fa-circle-o"></i> Kritik dan Saran</a></li>
          </ul>
        </li>
    </section>
    <!-- /.sidebar -->
  </aside>