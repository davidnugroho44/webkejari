<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @property    Rnquery rnquery
 */

class Panel_default
{
 public function __construct()
 {
  $this->CI =& get_instance();
 }

 public function _render_page($data = null, $output = null)
 {
  $this->isLogin();

   $this->CI->parser->parse($this->theme_admin() . 'header', $data);
   $this->CI->parser->parse($this->theme_admin() . 'nav', $data);
   $this->CI->parser->parse($data['viewspage'], $data);
   $this->CI->parser->parse($this->theme_admin() . 'footer', $data);
 }

 public function _render_default($data = null, $output = null)
 {
   $this->CI->parser->parse($data['viewspage'], $data);
 }

 public function theme_admin()
 {
  $set_theme='backend';
  $theme=$set_theme.'/';
  return  $theme;
 }
 public  function asset_admin()
 {
  $set_theme='backend';
  $theme=THEMES.$set_theme; // theme itu mengambil dari constant
  return  $theme;
 }

 public function isLogin() {
    if (!@$this->CI->session->userdata['email'] && !@$this->CI->session->userdata['username']) :
      redirect(BASE_URL('panel/login'));    
    endif;
 }

}