<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FPGrowth
{
    public $frequentItem;
    public $minimumSupportCount;
    public $minConfidence;
    public $supportCount;
    public $orderedFrequentItem;
    public $FPTree;
    public $countTransaksi;

    function __construct()
    {
        $this->frequentItem = array();
        // $this->minimumSupportCount = 2;
        // $this->minConfidence = 70;
        $this->supportCount 	= array();
        $this->orderedFrequentItem = array();
    }
    /*
    *input array of frequent pattern
    */

    public function setMinSupport($minSupport) {
        $this->minimumSupportCount = $minSupport;
    }

    public function setMinConf($minConf) {
        $this->minConfidence = $minConf;
    }

    public function setCountTransaksi($totalTransaksi) {
        $this->countTransaksi = $totalTransaksi;
    }

    public function set($t)
    {
        if(is_array($t))
        {
            $this->frequentItem[] 	= $t;
        }
    }
    public function get()
    {
        echo "<pre>";
        print_r($this->frequentItem);
        echo "</pre>";
    }
    public function getFrequentItem()
    {
        echo "<pre>";
        print_r($this->frequentItem);
        echo "</pre>";
    }
    public function orderFrequentItem($frequentItem, $supportCount)
    {
        foreach ($frequentItem as $k => $v) { //itemset priority
            $ordered 	= array();
            foreach ($supportCount as $key => $value) {
                if(isset($v[$key]))
                {
                    $ordered[$key]	= $v[$key];
                }
            }
            $this->orderedFrequentItem[$k]	= $ordered;
        }
    }
    public function getOrderedFrequentItem()
    {
        return $this->orderedFrequentItem;
    }
    public function countSupportCount()
    {
        if(is_array($this->frequentItem))
        {
            foreach ($this->frequentItem as $key => $value) {
                if(is_array($value))
                {
                    foreach ($value as $k => $v) {
                        if (empty($this->supportCount[$v])) {
                            $this->supportCount[$v] = 1;
                        }else{
                            $this->supportCount[$v] = $this->supportCount[$v] + 1;
                        }
                    }
                }
            }
        }
    }
    public function getSupportCount()
    {
        return $this->supportCount;
    }
    public function orderBySupportCount()
    {
        ksort($this->supportCount);
        arsort($this->supportCount);
    }
    public function removeByMinimumSupport($supportCount)
    {
        if(is_array($supportCount))
        {
            $this->supportCount = array();
            foreach ($supportCount as $key => $value) {
                if ($value >= $this->minimumSupportCount)
                {
                    $this->supportCount[$key] = $value;
                }
            }
        }
    }

    // public function getFPTree()
    // {
    //     echo "<pre>";
    //     print_r($this->FPTree);
    //     echo "</pre>";
    // }

//    OTHER
    public function buildFPTreeSingle($orderedFrequentItem)
    {
        $FPTree = array();
        $CFPTree = array();
        $CFPTreePrint = array();


        if(is_array($orderedFrequentItem))
        {
            $valBefore = 0; //nilai default parent
            foreach ($orderedFrequentItem as $orderedFrequentItemKey => $orderedFrequentItemValue) {
                $i 	= 0;
                foreach ($orderedFrequentItemValue as $key => $val):
                    if ($i == 0): //kondisi jika parent 0
                        $FPTree[] = array(
                            'item'  => $val,
                            'count' => 1,
                            'parent' => $i
                        );
                    else: //kondisi node dibawah 0
                        $FPTree[] = array(
                            'item'  => $val,
                            'count' => 1,
                            'parent' => $valBefore
                        );
                    endif;
                    if ($i == 0):
                        $valBefore = $val;
                    else:
                        $valBefore = $valBefore.','.$val;
                    endif;
                    $i++;
                endforeach;
            }
        }

        // array_print($FPTree);

        
        $dtParam = array_unique($FPTree, SORT_REGULAR); //menampilkan data, jika ada yg sama hanya 1 yg ditampilkan

        $count = 0;
        $index = 0;

        foreach ($dtParam as $key => $val):
            foreach ($FPTree as $keyCount => $valCount):
                if ($valCount['item'] == $val['item'] && $valCount['parent'] == $val['parent']):
                    $count = $count + $valCount['count'];

                   $FPTreeFinal[$index] = array(
                       'item'  => $valCount['item'],
                       'count' => $count,
                       'parent' => $valCount['parent']
                   );

                endif;
            endforeach;
            $count = 0;
            $index++;
        endforeach;

        // array_print($FPTreeFinal);

        // Proses seleksi by Support Count
        $FPTreePaternBase = array();
        $FPTreePaternBasePrint = array();
        foreach($this->getSupportCount() as $keySC => $valSC):            
            foreach($FPTreeFinal as $key => $val):
                if($keySC == $val['item']):
                    $FPTreeFinalSelected[] = array(
                        'item'  => $val['item'],
                        'count' => $val['count'],
                        'parent'=> $val['parent']    
                    );

                    $FPTreePaternBase[$keySC][] = array(
                        'parent'=> explode(",",$val['parent']),
                        'count' => $val['count']
                    );
                    if ($val['parent'] != 0) :
                        $FPTreePaternBasePrint[$keySC][] = '{'.$val['parent'].':'.$val['count'].'}'; //return
                    endif;
                endif;
            endforeach;
        endforeach;

        $CFPTreeTemp = array();
        foreach ($FPTreePaternBase as $keyPB => $valPB) { // PB -> Patern Base
            foreach ($valPB as $key => $val) {
                $count = $val['count'];
                foreach ($val['parent'] as $keyP => $valP) { // P -> Parent
                    if ($valP != 0):
                        $CFPTreeTemp[$keyPB][] = array(
                            'parent'    => $valP,
                            'count'     => $count
                        );
                    endif;
                }
            }
        }

        // array_print($CFPTreeTemp); //ConditionalPatternBase

        // $PTemp = null;
        $PTempBefore = null;
        $countTemp = 0;
        foreach ($CFPTreeTemp as $keyC => $valC) { // Conditional FPTree
            // start sort
                $parent  = array_column($valC, 'parent');
                $count = array_column($valC, 'count');

                array_multisort($parent, SORT_ASC, $valC);
            // end sort

            foreach ($valC as $key => $val) {
                if($PTempBefore != $val['parent']):
                    $countTemp = $val['count'];
                else:
                    $countTemp = $countTemp + $val['count'];
                endif;

                $CFPTree[$keyC][$val['parent']] = array(
                    'count' => $countTemp
                );

                $PTempBefore = $val['parent'];
            }
            $countTemp = 0;
        }

        // start create view CFPTree
        foreach ($CFPTree as $key => $val) {
            foreach ($val as $key2 => $val2) {
                $CFPTreePrint[$key][] = '{'.$key2.':'.$val2['count'].'}';
            }
        }
        // end create view CFPTree
        // array_print($CFPTreePrint); //ConditionalFPTree
        // start frequent item

        // start support single item(yang mengandung A)
        $FrequenItem = array();
        foreach ($this->supportCount as $key => $val) {
            $FrequenItemSingle[] = array(
                'parent'    => $key,
                'count'     => $val,
                'support'   => ($val / $this->countTransaksi) * 100
            );
        }
        // end support single item

        // start support itemset (yang mengandung A dan B)
        foreach ($CFPTree as $keyC => $valC) {
            foreach ($valC as $key => $val) {
                if ($val['count'] >= $this->minimumSupportCount) :
                    $FrequenItem[] = array(
                        'item'      => $keyC,
                        'parent'    => $keyC.','.$key,
                        'count'     => $val['count'],
                        'support'   => ($val['count'] / $this->countTransaksi) * 100
                    );
                endif;
            }
        }
        // end support itemset

        // array_print($this->countTransaksi);
        // array_print($FrequenItem);

        // start confidence & ratio lift
        $FrequenItemConf = array();

        foreach ($FrequenItem as $keyFI => $valFI) { // FI = Frequent Item
            foreach ($FrequenItemSingle as $keyS => $valS) { // S = Single
                if (explode(',',$valFI['parent'])[0] == $valS['parent']) { //(jml transaksi A / total transaksi)*100%
                    $supportA = $valS['support'];
                }
                if (explode(',',$valFI['parent'])[1] == $valS['parent']) {
                    $supportB = $valS['support'];
                }
            }

            $confidence = ($valFI['support'] / $supportA) * 100;

             // array_print($valFI['support']);

            if ($confidence >= $this->minConfidence) :
                $FrequenItemConf[] = array(
                    'item'          => $valFI['item'],
                    'parent'        => $valFI['parent'],
                    'count'         => $valFI['count'],
                    'support'       => $valFI['support'],
                    'confidence'    => $confidence,
                    'lift_ratio'    => ($valFI['support'] / ($supportA * $supportB)) * 100
            );
            endif;
        }

        // array_print($FrequenItemConf);
        // start create view Frequent Item Set
        $FrequenItemConfPrint = array();
        foreach ($FrequenItem as $key => $val) {
            $FrequenItemConfPrint[$val['item']][] = array(
                'parent'    => $val['parent'],
                'count'     => $val['count'],
            );
        }
        // end create view Frequent Item Set

        // end confidence & ration lift

        // start operasional
        $itemTemp = array();
        foreach ($FrequenItemConf as $key => $val) {
            $itemTemp[] = explode(',', $val['parent']); 
        }

        $item = array();
        foreach ($itemTemp as $key => $val) {
            foreach ($val as $key2 => $val2) {
                $item[] = $val2;
            }
        }
        $item = array_unique($item);
        // end operasional

        $return = array(
            'FPTreePaternBase' => $FPTreePaternBasePrint,
            'FrequenItemConfPrint' => $FrequenItemConfPrint,
            'FrequenItemConf' => $FrequenItemConf,
            'FPTree' => $CFPTreePrint,
            'itemOperasional' => $item,
        );

        // array_print($FrequenItemConf);

        return $return;
    }
}