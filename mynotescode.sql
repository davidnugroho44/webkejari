-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 28 Agu 2019 pada 18.04
-- Versi server: 10.1.37-MariaDB
-- Versi PHP: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mynotescode`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `bahan`
--

CREATE TABLE `bahan` (
  `id_bahan` int(11) NOT NULL,
  `nama_bahan` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bahan`
--

INSERT INTO `bahan` (`id_bahan`, `nama_bahan`) VALUES
(1, 'Beras'),
(3, 'Gula');

-- --------------------------------------------------------

--
-- Struktur dari tabel `komposisi`
--

CREATE TABLE `komposisi` (
  `id_komposisi` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_bahan` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `komposisi`
--

INSERT INTO `komposisi` (`id_komposisi`, `id_menu`, `id_bahan`, `jumlah`) VALUES
(3, 1, 1, 200),
(4, 1, 3, 20);

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL,
  `nama_menu` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `menu`
--

INSERT INTO `menu` (`id_menu`, `nama_menu`) VALUES
(1, 'Nasi Goreng Sosis'),
(2, 'Coklat Float'),
(3, 'Kopi Susu'),
(4, 'Indomie'),
(5, 'Es Teh'),
(6, 'Rice Bowl Ayam'),
(7, 'Nasi Ayam Telur Sambel Matah'),
(8, 'Nasi Chicken Spicy'),
(9, 'Thai Tea'),
(10, 'Kopi Susu'),
(11, 'Expresso'),
(12, 'Lemon Squash'),
(16, 'Afogato');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `no_nota` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `tgl_transaksi`, `no_nota`, `id_menu`, `jumlah`) VALUES
(23, '2019-08-28', 1, 1, 1),
(24, '2019-08-28', 1, 2, 1),
(25, '2019-08-28', 1, 3, 2),
(26, '2019-08-28', 1, 4, 1),
(27, '2019-08-28', 2, 3, 1),
(28, '2019-08-28', 2, 2, 2),
(29, '2019-08-28', 2, 1, 1),
(30, '2019-08-28', 3, 6, 1),
(31, '2019-08-28', 3, 4, 1),
(32, '2019-08-28', 3, 9, 3),
(33, '2019-08-28', 4, 7, 1),
(34, '2019-08-28', 4, 5, 1),
(35, '2019-08-28', 5, 5, 1),
(36, '2019-08-28', 5, 10, 1),
(37, '2019-08-28', 6, 5, 1),
(38, '2019-08-28', 6, 8, 1),
(39, '2019-08-28', 6, 9, 1),
(40, '2019-08-28', 7, 1, 1),
(41, '2019-08-28', 7, 11, 2),
(42, '2019-08-28', 7, 12, 1),
(43, '2019-08-28', 8, 1, 1),
(44, '2019-08-28', 8, 4, 1),
(45, '2019-08-28', 8, 5, 1),
(46, '2019-08-28', 9, 1, 1),
(47, '2019-08-28', 9, 16, 1),
(48, '2019-08-28', 9, 1, 1),
(49, '2019-08-28', 9, 16, 1),
(50, '2019-08-28', 10, 5, 2),
(51, '2019-08-28', 10, 7, 1),
(52, '2019-08-28', 10, 8, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`) VALUES
(8, 'david', '', 'davidnugroho44@gmai.com'),
(12, 'Kevin', '', 'kevin@lasn');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `bahan`
--
ALTER TABLE `bahan`
  ADD PRIMARY KEY (`id_bahan`);

--
-- Indeks untuk tabel `komposisi`
--
ALTER TABLE `komposisi`
  ADD PRIMARY KEY (`id_komposisi`);

--
-- Indeks untuk tabel `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `bahan`
--
ALTER TABLE `bahan`
  MODIFY `id_bahan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `komposisi`
--
ALTER TABLE `komposisi`
  MODIFY `id_komposisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
